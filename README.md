# My i3 Build

![i3](/uploads/34fb0926f56925fe2b6d89cca96e3ec0/i3.png)

This repository contains my personal build of i3-Gaps.

The included files contain an i3 configuration and startup file.  You can include startup items in the configuration itself but I prefer to keep these in a seperate file.

There is also a polybar directory with an i3config file, two polybar launch scripts and a scripts folder.  The multimontor launch script should suit most situations, but if you prefer to use the standard launch script please edit the i3 config file and the monitor and tray sections in the polybar i3config file to suit, e.g. change MONTOR1 and MONITOR2 to MONITOR and remove the tray environment variable from the tray-position item and replace with trayposition = right.

Feel free to use these builds.  I do not, however, offer or imply any form of support or ongoing maintenance.  And of course, you use them entirely at your own risk.


## License
The content of this repository is licensed under the MIT Licence. Basically, that gives you the right to use, copy, modify, etc. the content how you see fit. You can read the full licence terms here (https://opensource.org/licenses/MIT).

